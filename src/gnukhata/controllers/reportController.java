package gnukhata.controllers;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlrpc.XmlRpcException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import gnukhata.globals;
import gnukhata.views.AccountReport;
import gnukhata.views.LedgerRecon;
import gnukhata.views.ViewBalanceSheetReport;
import gnukhata.views.ViewCashFlowReport;
import gnukhata.views.ViewDualLedgr;
import gnukhata.views.ViewLedgerReport;
import gnukhata.views.ViewSourcesOfFundBalanceSheet;
import gnukhata.views.viewBalanceSheet;
import gnukhata.views.viewCashflow;
import gnukhata.views.viewProfitAndLossReport;
import gnukhata.views.viewProjectStatementReport;
import gnukhata.views.viewReconciliation;
import gnukhata.views.viewTrialBalReport;
import gnukhata.views.viewextendedtrialbalreport;
import gnukhata.views.viewgrosstrialbalreport;

public class reportController {
	
	public static void showBalanceSheet (Composite grandParent, String endDate, String tbType)
	{
		List<Object> serverParams = new ArrayList<Object>();
		List<Object> serverParams2 = new ArrayList<Object>();
		serverParams2.add(new Object[]{globals.session[2],globals.session[2],endDate});
		serverParams2.add(globals.session[0]);
		serverParams.add(new Object[]{globals.session[2], globals.session[2] ,endDate});
		serverParams.add(globals.session[0]);
		try {
			Object[] profitloss = (Object[]) globals.client.execute("reports.getProfitLoss", serverParams2);
			Object[] result = (Object[]) globals.client.execute("reports.getBalancesheet" , serverParams);
			if(tbType.equals("Conventional Balance Sheet"))
			{
			
				ViewBalanceSheetReport bsr = new ViewBalanceSheetReport(grandParent, SWT.NONE, endDate, result ,profitloss);
				bsr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			if(tbType.equals("Sources & Application of fund"))
			{
				ViewSourcesOfFundBalanceSheet fbsr = new ViewSourcesOfFundBalanceSheet(grandParent, SWT.NONE, endDate, result, profitloss);
				fbsr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
		

		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		
		
	}

	public static void showTrialBalance (Composite grandParent, String endDate, String tbType)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{globals.session[2], globals.session[2], endDate});
		serverParams.add(globals.session[0]);
		try {
			if(tbType.equals("Net Trial Balance"))
			{
				
				Object[] tbData = (Object[]) globals.client.execute("reports.getTrialBalance", serverParams);
				viewTrialBalReport tbr = new viewTrialBalReport(grandParent, endDate, SWT.NONE , tbData);
				tbr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			if(tbType.equals("Gross Trial Balance"))
			{
				
				Object[] tbData1 = (Object[]) globals.client.execute("reports.getGrossTrialBalance", serverParams);
				viewgrosstrialbalreport tbr = new viewgrosstrialbalreport(grandParent,endDate, SWT.NONE , tbData1);
				tbr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			if(tbType.equals("Extended Trial Balance"))
			{
				
				Object[] tbData2 = (Object[]) globals.client.execute("reports.getExtendedTrialBalance", serverParams);
				viewextendedtrialbalreport tbr = new viewextendedtrialbalreport(grandParent,endDate, SWT.NONE , tbData2);
				tbr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			

		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		
	}
	
	
	public static void showProfitAndLoss(Composite grandParent,String toDate)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{globals.session[2], globals.session[2],toDate});
		
		
		serverParams.add(globals.session[0]);
		try {
			Object[] result = (Object[]) globals.client.execute("reports.getProfitLoss" , serverParams);
			viewProfitAndLossReport vplr=new viewProfitAndLossReport(grandParent, SWT.NONE, toDate, result);
			vplr.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
		} 
		catch (XmlRpcException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void showProjectStatement(Composite grandParent,String toDate, String selectproject)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{selectproject,globals.session[2], globals.session[2], toDate});
		
		
		serverParams.add(globals.session[0]);
		try {
			Object[] result = (Object[]) globals.client.execute("reports.getProjectStatement" , serverParams);
			viewProjectStatementReport vprs = new viewProjectStatementReport(grandParent, toDate, SWT.NONE, result, selectproject);
			vprs.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
		} 
		catch (XmlRpcException e)
		{
			e.printStackTrace();
		}
	}
	public static void showLedger(Composite grandParent, String accountName,String fromDate,String toDate,String ProjectName, boolean narrationFlag, boolean tbDrillDown,boolean psdrilldown, String tbType,String selectproject)
	{
		
					
		ArrayList<Object> serverParams = new ArrayList<Object>();
		//code for sending project name back to ledger report
		serverParams.add(new Object[]{accountName,fromDate,toDate,globals.session[2],ProjectName});
		serverParams.add(globals.session[0]);
			
			try {
					Object[] result_f = (Object[]) globals.client.execute("reports.getLedger", serverParams);
					String oldselectproject = null;
					String oldenddate = null;
					String oldprojectname = null;
					boolean narration =false;
					String oldaccname = null;
					String oldfromdate = null ;
					boolean dualledgerflag=false;
					ViewLedgerReport ledger = new ViewLedgerReport(grandParent, SWT.None, result_f,ProjectName,oldprojectname, narrationFlag,narration, accountName,oldaccname,fromDate,oldfromdate,toDate,oldenddate,tbDrillDown,psdrilldown,tbType,selectproject,oldselectproject,dualledgerflag);
			
					System.out.print("Project name is :"+ProjectName);
			//now make an instance of the ledgerReport which is a composite.
			//in the constructor pass this result as a parameter.
			// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
	}
		
		
		}
	
	
	
	public static void showDualLedger(Composite grandParent, String accountName,String oldaccName,String fromDate,String oldfromdate,String toDate,String oldenddate,String ProjectName,String oldprojectName, boolean narrationFlag,boolean narration, boolean tbDrillDown,boolean tbflag,boolean psdrilldown,boolean projectflag, String tbType,String tb,String selectproject,String oldselectproject,boolean dualledgerflag,boolean dualflag)
	{
		
		
		ArrayList<Object> serverParams = new ArrayList<Object>();
		//code for sending project name back to ledger report
		serverParams.add(new Object[]{accountName,fromDate,toDate,globals.session[2],ProjectName});
		serverParams.add(globals.session[0]);
		ArrayList<Object> serverParams1 = new ArrayList<Object>();
		//code for sending project name back to ledger report
		serverParams1.add(new Object[]{oldaccName,oldfromdate,oldenddate,globals.session[2],oldprojectName});
		serverParams1.add(globals.session[0]);
		
		try {
				Object[] result_t1 = (Object[]) globals.client.execute("reports.getLedger", serverParams);
				Object[] result_t2 = (Object[]) globals.client.execute("reports.getLedger", serverParams1);
				
				ViewDualLedgr ledger_t1 = new ViewDualLedgr(grandParent, SWT.None, result_t1,result_t2,ProjectName,oldprojectName, narrationFlag,narration, accountName,oldaccName,fromDate,oldfromdate,toDate,oldenddate,tbDrillDown,tbflag,psdrilldown,projectflag,tbType,tb,selectproject,oldselectproject,dualledgerflag,dualflag);
						
				System.out.print("Project name is :"+ProjectName);
		//now make an instance of the ledgerReport which is a composite.
		//in the constructor pass this result as a parameter.
		// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
		
		
		
	}

			
		public static void showCashFlow(Composite grandParent, String fromDate, String toDate, String financialFrom)
		{
			ArrayList<Object> serverParams = new ArrayList<Object>();
			serverParams.add(new Object[]{fromDate,toDate,financialFrom});
			serverParams.add(globals.session[0]);
			try {
				Object[] result = (Object[]) globals.client.execute("reports.getCashFlow" , serverParams);
				ViewCashFlowReport CashFlow = new ViewCashFlowReport(grandParent, SWT.NONE, result, fromDate,toDate, financialFrom);
				
			} catch (XmlRpcException e)
			{
				e.printStackTrace();
			}
		}
		
		public static void getAccountReport(Composite grandParent)
		{
			try
			{
				Object[] result=(Object[])globals.client.execute("account.getAccountReport",new Object[]{globals.session[0]});
				AccountReport ar=new AccountReport(grandParent, SWT.NONE, result);
				
			}
			catch (XmlRpcException e)
			{
				e.printStackTrace();
			}
			
		}
	
		public static String[] getBankList()
		{
			try
			{
				Object[] result = (Object[]) globals.client.execute("reports.getBankList",new Object[]{globals.session[0]});
				String[] accounts = new String[result.length];
				for(int i = 0; i<result.length; i++)
				{

					accounts[i] = result[i].toString();
				}
				return accounts;
			}
			catch(Exception e)
			{
				e.getMessage();
				return new String[]{};
			}
		}


		
		public static void showledgerRecon(Composite grandParent, String bankname,String fromDate,String toDate, String projname,boolean narrationFlag)
		{
			
						
			ArrayList<Object> serverParams = new ArrayList<Object>();
			//code for sending project name back to ledger report
			serverParams.add(new Object[]{bankname,fromDate,toDate,globals.session[2],projname});
			serverParams.add(globals.session[0]);
			boolean narration =false;
				
				try {
						Object[] result_f = (Object[]) globals.client.execute("reports.getReconLedger", serverParams);
						
						LedgerRecon lr = new LedgerRecon(grandParent,bankname,fromDate,toDate, SWT.None, result_f,narrationFlag);
					
						//System.out.print("Project name is :"+ProjectName);
				//now make an instance of the ledgerReport which is a composite.
				//in the constructor pass this result as a parameter.
				// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
		}
			
			
		}
		
		public static void getUnclearedAccounts(Composite grandParent, String bankname,String fromDate,String toDate,String projname,boolean narration_Flag,boolean cleared_Flag) {
			ArrayList<Object> serverParams = new ArrayList<Object>();
			serverParams.add(new Object[]{bankname,fromDate,toDate,globals.session[2],projname});
			serverParams.add(globals.session[0]);
			boolean narrationflag = narration_Flag;
			boolean clearedflag = cleared_Flag;
				
				try {	
					
						Object[] unclearResut = (Object[]) globals.client.execute("reports.updateBankRecon", serverParams);
								Integer length = unclearResut.length - 7;
								Object[] unclear;
								for (int i = 0; i < length; i++) {
									unclear = (Object[]) unclearResut[i];

									MessageBox msg=new MessageBox(new Shell(),SWT.OK);
									msg.setMessage("unclear data"+unclear[i].toString());
									msg.open();
								}
								
								
		/*						for row in unclear:
									row.append("") #clearance date
									row.append("") #memo
								//now, get ledgerdata depending upon user requirements.(with cleared transactions or withaut cleared transactions)
								if request.params["cleared_acc_flag"] == 'false':
									c.ledgerdata = unclear
									c.length = len(unclear)
								else:
									# get ledger for the given period
									#tran_date, acc_name, ref_no, voucher_code, dbt_amount, cdt_amount, narration, clearance_date, memo (9 fields)
									ledger = app_globals.server_proxy.reports.getReconLedger([request.params["accountname"],fromdate,todate,financialStart,'No Project'],session["gnukhata"])
									voucher_list = []
									for v_code in unclear:
										voucher_list.append(v_code[3])
									for row in ledger:
											if row[3] not in voucher_list : #row[3] is v_code of ledger list
												unclear.append(row)
									
									for row in unclear: #split clearance date into date, month, year
										row.append(str(row[7])[0:2])
										row.append(str(row[7])[3:5])
										row.append(str(row[7])[6:10])
									#unclear list contains tran_date, acc_name, ref_no, voucher_code, dbt_amount, cdt_amount, narration, clearance_date, memo (9 fields), c(date), c(moth), c(year)
									newunclearList = sorted(unclear,key=lambda x: datetime.datetime.strptime(x[0],'%d-%m-%Y'))
									c.ledgerdata = newunclearList
									c.length = len(unclear)
									*/
						//LedgerRecon lr = new LedgerRecon(grandParent,bankname,fromDate,toDate, SWT.None, result_f,narrationFlag);
				
		
				}catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public static void showUpdateRecon(Composite grandParent, String bankname,String fromDate,String toDate, String projname,boolean narrationFlag)
		{
						
			ArrayList<Object> serverParams = new ArrayList<Object>();
			//code for sending project name back to ledger report
			serverParams.add(new Object[]{bankname,fromDate,toDate,globals.session[2],projname});
			serverParams.add(globals.session[0]);
			boolean narration =false;
				
				try {
						Object[] result_f = (Object[]) globals.client.execute("reports.updateBankRecon", serverParams);
						
						//LedgerRecon lr = new LedgerRecon(grandParent,bankname,fromDate,toDate, SWT.None, result_f,narrationFlag);
				
						//System.out.print("Project name is :"+ProjectName);
				//now make an instance of the ledgerReport which is a composite.
				//in the constructor pass this result as a parameter.
				// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
		}
			
			
		}
				
		

}
