package gnukhata.views;

import gnukhata.controllers.accountController;
import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.transactionController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.jopendocument.sample.SpreadSheetCreation;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;
/*import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
*/
import com.sun.org.apache.bcel.internal.generic.NEW;

public class getUnclearedTransactions extends Composite {

	String strOrgName;
	String strFromYear;
	String strToYear;
	String accname;
	int counter=0;
	boolean narration1;

	Table ledgerReconReport;
	TableColumn Srno;
	TableColumn Date;
	TableColumn Particulars;
	TableColumn VoucherNumber;
	TableColumn Dr;
	TableColumn Cr;
	TableColumn Narration;
	TableColumn Clrdate;
	TableColumn Memo;
	TableItem headerRow;
	Label lblOrgDetails;
	Label lblheadline;
	Label lblorgname;
	Label lblsrno;
	Label lblDate;
	Label lblParticulars;
	Label lblVoucherNumber;
	Label lblNarration;
	Label lblDr;
	Label lblCr;
	Label lblclrdate;
	Label lblmemo;
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblPageName;
	Label lblPeriod;
	TableEditor srnoeditor;
	TableEditor DateEditor;
	TableEditor ParticularEditor;
	TableEditor VoucherNumberEditor;
	TableEditor DrEditotr;
	TableEditor CrEditor;
	TableEditor clreditor;
	TableEditor memoeditor;
	
	TableEditor NarrationEditor;
	static Display display;
	
	Button btnViewLedgerReconLedgerRecon;
	//Button btnViewLedgerReconLedgerForAccount;
	Button btnRecon;
	//Button btnViewLedgerReconDualLedger;
	Label cleareditem;
	Button Btnclear;
	//String tb;
	String strFromDate;
	String strToDate;
	//ArrayList<Button> voucherCodes = new ArrayList<Button>();
	
	String bankname="";
	String startDate="";
	String endDate="";
	String projectName;
	String ledgerProject;
	//boolean narration1;
	boolean tbflag;
	boolean projectflag;
	boolean ledgerflag;
	boolean dualflag;
	String oldaccname;
	String oldfromdate;
	String oldenddate;
	String oldselectproject;
	String oldprojectname;
	boolean oldnarration;
	Button btnok;
	ArrayList<Text> txtcleardate = new ArrayList<Text>();
	
	ArrayList<Text>  txtmemo= new ArrayList<Text>();
	//Vector<Object> printLedgerData = new Vector<Object>();
	public getUnclearedTransactions(Composite parent,String selectbank,String toDate,String fromdate,int style, Object[] result,boolean narrationFlag, boolean clearedFlag) {
		super(parent, style);
		// TODO Auto-generated constructor stub
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		//txtaccname.setText(result[0].toString());
		
		/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
		msg.setMessage("Project name"+selectproject);
		msg.open();*/
		narration1= narrationFlag;
		bankname=selectbank;
		
		//old values
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout =new FormData();
	    strFromDate=fromdate.substring(8)+"-"+fromdate.substring(5,7)+"-"+fromdate.substring(0,4);
		strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);
	  
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();

		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(70);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(img);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
		 lblheadline=new Label(this, SWT.NONE);
		lblheadline.setText(""+globals.session[1]);
		lblheadline.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(35);
		lblheadline.setLayoutData(layout);
	

		
		Label lblAccName=new Label(this, SWT.NONE);
		lblAccName.setFont( new Font(display,"Times New Roman", 14, SWT.NORMAL | SWT.BOLD) );
		/*if(! ProjectName.equals("No Project"))
		{*/
		lblAccName.setText("Bank Name: "+bankname);
		/*}*/
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(5);
		lblAccName.setLayoutData(layout);
		

		lblPageName = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
		lblPageName.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		lblPageName.setText("Period From "+strFromDate+" To "+strToDate);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(60);
	
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		lblPageName.setLayoutData(layout);
		
		cleareditem  = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
		cleareditem.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		cleareditem.setText("CHECKBOX FOR VIEWING CLEARED ITEMS");
		layout = new FormData();
		layout.top = new FormAttachment(lblPageName,1);
		layout.left = new FormAttachment(25);
	
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		cleareditem.setLayoutData(layout);
	
		Btnclear=new Button(this, SWT.CHECK);
		Btnclear.setText("Cleared Transactions");
		layout = new FormData();
		layout.top=new FormAttachment(cleareditem, 1);
		layout.left = new FormAttachment(30);
		Btnclear.setLayoutData(layout);
		Btnclear.setVisible(true);
		
		btnok =new Button(this,SWT.PUSH);
		btnok.setText(" OK ");
		btnok.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(Btnclear,1);
		layout.left=new FormAttachment(35);
		btnok.setLayoutData(layout);
		
		

	Label lblunclr = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
	lblunclr.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
	lblunclr.setText("UNCLEARED ACCOUNTS");
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(50);
	
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		lblunclr.setLayoutData(layout);


	
	    ledgerReconReport= new Table(this, SWT.MULTI|SWT.BORDER|SWT.FULL_SELECTION|SWT.LINE_SOLID);
	    ledgerReconReport.setLinesVisible (true);
		ledgerReconReport.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblunclr,10);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(90);
		layout.bottom = new FormAttachment(80);
		ledgerReconReport.setLayoutData(layout);
		
		
				
		btnRecon =new Button(this,SWT.PUSH);
		btnRecon.setText(" SAVE ");
		btnRecon.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReconReport,8);
		layout.left=new FormAttachment(20);
		btnRecon.setLayoutData(layout);
		
		
		btnViewLedgerReconLedgerRecon=new Button(this, SWT.PUSH);
		btnViewLedgerReconLedgerRecon.setText(" Back To View Reconciliation");
		btnViewLedgerReconLedgerRecon.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReconReport,8);
		layout.left=new FormAttachment(btnRecon,20);
		btnViewLedgerReconLedgerRecon.setLayoutData(layout);
		

				
	  
	    this.makeaccssible(ledgerReconReport);
	    this.getAccessible();
	    this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
	    this.setReport(result);
	    //this.pack();
	    this.setEvents();
	   

	}
	public void makeaccssible(Control c)
	{
		c.getAccessible();
	}
	private void setReport(Object[]result)
	{
		
		
		lblDate = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblDate.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblDate.setText("    Date   ");
		
		lblParticulars = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblParticulars.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblParticulars = new Label(ledgerReconReport,SWT.BORDER);
		lblParticulars.setText("    		Particulars		     ");
		
		lblVoucherNumber = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblVoucherNumber.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblVoucherNumber = new Label(ledgerReconReport,SWT.BORDER);
		lblVoucherNumber.setText(" V.Number");
		
		lblDr = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblDr.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblDr = new Label(ledgerReconReport,SWT.BORDER);
		lblDr.setText("    		Debit		     ");
		
		lblCr = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblCr.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblCr = new Label(ledgerReconReport,SWT.BORDER);
		lblCr.setText("    		Credit		     ");
		
		lblNarration = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblNarration.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblNarration.setText("   Narration   ");
		
		lblclrdate = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblclrdate.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblclrdate.setText("  Clearance Date   ");
		
		lblmemo = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblmemo.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblmemo.setText("   Memo   ");
		
		  final int ledgwidth = ledgerReconReport.getClientArea().width;
		  
		//lblclrdate
		 
	    ledgerReconReport.addListener(SWT.MeasureItem, new Listener() 
		{
	    	@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				// Srno.setWidth(5 * ledgwidth / 100);
	    		if(narration1==true)
				{
				
					Date.setWidth(15 * ledgwidth / 100);
					Particulars.setWidth(25 * ledgwidth / 100);
					VoucherNumber.setWidth(10 * ledgwidth / 100);
					Dr.setWidth(10 * ledgwidth / 100);
					Cr.setWidth(10 * ledgwidth / 100);
					Narration.setWidth(25 * ledgwidth / 100);
					Clrdate.setWidth(15 * ledgwidth / 100);
					Memo.setWidth(15 * ledgwidth / 100);
				}
	    		else
				{
					Date.setWidth(15 * ledgwidth / 100);
					Particulars.setWidth(35 * ledgwidth / 100);
					VoucherNumber.setWidth(10 * ledgwidth / 100);
					Dr.setWidth(20 * ledgwidth / 100);
					Cr.setWidth(20 * ledgwidth / 100);
					Clrdate.setWidth(15 * ledgwidth / 100);
					Memo.setWidth(15 * ledgwidth / 100);
		
				}


								
				event.height = 11;
			    
			    
			};
		});
		
	    headerRow = new TableItem(ledgerReconReport, SWT.NONE);
		TableItem[] items = ledgerReconReport.getItems();
		//Srno = new TableColumn(ledgerReconReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
		Date = new TableColumn(ledgerReconReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
		Particulars = new TableColumn(ledgerReconReport, SWT.BORDER);
		VoucherNumber = new TableColumn(ledgerReconReport, SWT.RIGHT);
		Dr= new TableColumn(ledgerReconReport, SWT.RIGHT);
		Cr= new TableColumn(ledgerReconReport, SWT.RIGHT);
		if(narration1)
		{
			Narration=new TableColumn(ledgerReconReport, SWT.BORDER);
		}
		Clrdate= new TableColumn(ledgerReconReport, SWT.RIGHT);
		Memo= new TableColumn(ledgerReconReport, SWT.RIGHT);

		
			
		if(narration1==true)
		{
		
	    DateEditor = new TableEditor(ledgerReconReport);
		DateEditor.grabHorizontal = true;
		DateEditor.setEditor(lblDate,items[0],0);
		
		ParticularEditor = new TableEditor(ledgerReconReport);
		ParticularEditor.grabHorizontal = true;
		ParticularEditor.setEditor(lblParticulars,items[0],1);
		
		VoucherNumberEditor = new TableEditor(ledgerReconReport);
		VoucherNumberEditor.grabHorizontal = true;
		VoucherNumberEditor.setEditor(lblVoucherNumber,items[0],2);
		
		DrEditotr = new TableEditor(ledgerReconReport);
		DrEditotr.grabHorizontal = true;
		DrEditotr.setEditor(lblDr,items[0],3);
		
		CrEditor = new TableEditor(ledgerReconReport);
		CrEditor.grabHorizontal = true;
		CrEditor.setEditor(lblCr,items[0],4);
		NarrationEditor = new TableEditor(ledgerReconReport);
		NarrationEditor.grabHorizontal = true;
		NarrationEditor.setEditor(lblNarration,items[0],5);
		
		
		clreditor = new TableEditor(ledgerReconReport);
		clreditor.grabHorizontal = true;
		clreditor.setEditor(lblclrdate,items[0],6);
		memoeditor = new TableEditor(ledgerReconReport);
		memoeditor.grabHorizontal = true;
		memoeditor.setEditor(lblmemo,items[0],7);

		//Srno.pack();
		Date.pack();
		Particulars.pack();
		VoucherNumber.pack();
	    Dr.pack();
		Cr.pack();
		
		Narration.pack();
		Clrdate.pack();
		Memo.pack();
		}
		else
		{
			

		    DateEditor = new TableEditor(ledgerReconReport);
			DateEditor.grabHorizontal = true;
			DateEditor.setEditor(lblDate,items[0],0);
			
			ParticularEditor = new TableEditor(ledgerReconReport);
			ParticularEditor.grabHorizontal = true;
			ParticularEditor.setEditor(lblParticulars,items[0],1);
			
			VoucherNumberEditor = new TableEditor(ledgerReconReport);
			VoucherNumberEditor.grabHorizontal = true;
			VoucherNumberEditor.setEditor(lblVoucherNumber,items[0],2);
			
			DrEditotr = new TableEditor(ledgerReconReport);
			DrEditotr.grabHorizontal = true;
			DrEditotr.setEditor(lblDr,items[0],3);
			
			CrEditor = new TableEditor(ledgerReconReport);
			CrEditor.grabHorizontal = true;
			CrEditor.setEditor(lblCr,items[0],4);
			/*NarrationEditor = new TableEditor(ledgerReconReport);
			NarrationEditor.grabHorizontal = true;
			NarrationEditor.setEditor(lblNarration,items[0],5);
			*/
			
			clreditor = new TableEditor(ledgerReconReport);
			clreditor.grabHorizontal = true;
			clreditor.setEditor(lblclrdate,items[0],5);
			
			memoeditor = new TableEditor(ledgerReconReport);
			memoeditor.grabHorizontal = true;
			memoeditor.setEditor(lblmemo,items[0],6);

			//Srno.pack();
			Date.pack();
			Particulars.pack();
			VoucherNumber.pack();
		    Dr.pack();
			Cr.pack();
			
			//Narration.pack();
			Clrdate.pack();
			Memo.pack();
		}
				
	}
	
	public void setEvents()
	{
		
				
		
		btnViewLedgerReconLedgerRecon.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnRecon.setFocus();
				}
				
			}
		});
		
		
		btnRecon.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewLedgerReconLedgerRecon.setFocus();
				}
				
			}
		});
		
		btnRecon.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				//Object[] result;
				Composite grandParent = (Composite) btnRecon.getParent().getParent();
				btnRecon.getParent().dispose();
					
					//Object[] result=null;
					//EditReconcile e=new EditReconcile(grandParent, SWT.NONE);
					//se.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});

		
		btnViewLedgerReconLedgerRecon.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewLedgerReconLedgerRecon.getParent().getParent();
				btnViewLedgerReconLedgerRecon.getParent().dispose();
					
					viewReconciliation vl=new viewReconciliation(grandParent,SWT.NONE,false);
					vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
			ledgerReconReport.setFocus();
			}
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}

}
	

	