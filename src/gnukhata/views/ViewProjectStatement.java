package gnukhata.views;

import gnukhata.globals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Text;

public class ViewProjectStatement extends Composite{
	static String strOrgName;
	static String strFromYear;
	static String strToYear;
	
	public static String typeFlag;
	static Display display;
	Text txtddate;
	Text txtmdate;
	Text txtyrdate;
	Label dash1;
	Label dash2;
	TabFolder tfTransaction;
	Combo drpdwnselectproj;
	Button btnView;
	String searchText = "";
	long searchTexttimeout = 0;
	long wait=0;
	
	
	Vector<Object> params;
	protected int[] orgNameList;
	public ViewProjectStatement(Composite parent,int style)
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
		    
		strToYear =  globals.session[3].toString();
		
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(60);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(img);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("---------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
			
		Label lblviewprojstmt = new Label(this, SWT.NONE);
		lblviewprojstmt.setText("View Project Statement");
		lblviewprojstmt.setFont(new Font(display, "Times New Roman", 20, SWT.BOLD|SWT.UNDERLINE_SINGLE));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,25);
		layout.left = new FormAttachment(32);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblviewprojstmt.setLayoutData(layout);
		

				
		Label lblperiod = new Label(this,SWT.NONE);
		lblperiod.setFont( new Font(display,"Times New Roman", 16, SWT.NONE ) );
		lblperiod.setText("\n\n"+"Period: "+"From: "+globals.session[2]);
		layout = new FormData();
		layout.top = new FormAttachment(lblviewprojstmt,20);
		layout.left = new FormAttachment(32);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblperiod.setLayoutData(layout);
		
		Label lblto =new Label(this , SWT.NONE);
		lblto.setFont(new Font(display,"Times New Roman", 16, SWT.NONE ));
		lblto.setText("T&o:");
		layout = new FormData();
		layout.top = new FormAttachment(lblperiod,20);
		layout.left = new FormAttachment(40);
		lblto.setLayoutData(layout);
	
		//Composite this = new Composite(tfTransaction,SWT.BORDER);
		//this.setLayout(formlayout);
		txtddate = new Text(this,SWT.BORDER);
		txtddate.setFocus();
	//	txtddate.setMessage("dd");
		txtddate.setText(strToYear.substring(0,2));
		txtddate.setSelection(0,2);
		txtddate.setTextLimit(2);
		layout = new FormData();
		layout.top = new FormAttachment(lblperiod,20);
		layout.left = new FormAttachment(43);
		layout.right = new FormAttachment(txtddate,30);
		//layout.bottom = new FormAttachment();
		txtddate.setFocus();
		txtddate.selectAll();
		txtddate.setLayoutData(layout);
		
		
		dash1 = new Label(this,SWT.NONE);
		dash1.setText("-");
		dash1.setFont(new Font(display, "Time New Roman",16,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblperiod,20);
		layout.left = new FormAttachment(txtddate , 2
				);
		layout.right = new FormAttachment(47);
		//layout.bottom = new FormAttachment(9);
		dash1.setLayoutData(layout);
		
		txtmdate = new Text(this,SWT.BORDER);
		//txtmdate.setMessage("mm");
		txtmdate.setText(strToYear.substring(3,5));
		txtmdate.setTextLimit(2);
		layout = new FormData();
		layout.top = new FormAttachment(lblperiod,20);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(txtmdate,30);
		//layout.bottom = new FormAttachment(9);
		txtmdate.setLayoutData(layout);
		
		dash2 = new Label(this,SWT.NONE);
		dash2.setText("-");
		dash2.setFont(new Font(display, "Time New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblperiod,20);
		layout.left = new FormAttachment(txtmdate,2);
		layout.right = new FormAttachment(51);
		//layout.bottom = new FormAttachment(9);
		dash2.setLayoutData(layout);
		
		txtyrdate = new Text(this,SWT.BORDER);
		/*txtyrdate.setMessage("yyyy");*/
		txtyrdate.setText(strToYear.substring(6));
		txtyrdate.setTextLimit(4);
		layout = new FormData();
		layout.top = new FormAttachment(lblperiod,20);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(txtyrdate,50);
		//layout.bottom = new FormAttachment(9);
		txtyrdate.setLayoutData(layout);
		
		Label lblselectproj = new Label(this, SWT.NONE);
		lblselectproj.setText("&Select Project:");
		lblselectproj.setFont(new Font(display, "Times New Roman", 16, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(txtyrdate,20);
		layout.left = new FormAttachment(32);
		lblselectproj.setLayoutData(layout);
		
		drpdwnselectproj = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
		layout = new FormData();
		layout.top = new FormAttachment(txtddate,20);
		layout.left = new FormAttachment(lblselectproj,2);
		layout.right = new FormAttachment(drpdwnselectproj,190);
		//layout.bottom = new FormAttachment(14);
		drpdwnselectproj.setLayoutData(layout);
		drpdwnselectproj.add("--Please select--");
		String[] allProjects = gnukhata.controllers.transactionController.getAllProjects();
		for (int i = 0; i < allProjects.length; i++ )
		{
			drpdwnselectproj.add(allProjects[i]);
		}
		drpdwnselectproj.select(0);
		
		btnView  =new Button(this,SWT.PUSH);
		btnView.setText("&View");
		btnView.setEnabled(false);
		btnView.setFont(new Font(display, "Times New Roman", 16, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(drpdwnselectproj,60);
		layout.left = new FormAttachment(45);
		//layout.right=new FormAttachment(60);
		btnView.setLayoutData(layout);
		//btnview.setVisible(true);
	
 
 

	this.getAccessible();
		this.setEvents();
		this.pack();
	//	this.open();
	//	this.showView();
	}
//the following method sets (registers) all the necesary event listeners on the respective widgets.
//this method will be the last call inside the constructor.

	

	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}

private void setEvents(){
	
	drpdwnselectproj.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			//code here
			if(arg0.keyCode== SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
			{
				if(drpdwnselectproj.getSelectionIndex()> 0  )
				{
				
				btnView.setFocus();
				return;
				}
			}
			long now = System.currentTimeMillis();
			if (now > searchTexttimeout){
		         searchText = "";
		      }
			searchText += Character.toLowerCase(arg0.character);
			searchTexttimeout = now + 1000;					
			for(int i = 0; i < drpdwnselectproj.getItemCount(); i++ )
			{
				if(drpdwnselectproj.getItem(i).toLowerCase().startsWith(searchText ) ){
					//arg0.doit= false;
					drpdwnselectproj.select(i);
					drpdwnselectproj.notifyListeners(SWT.Selection ,new Event()  );
				}
			}
		}
	});

	txtddate.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			if(txtddate.getText().length()==txtddate.getTextLimit())
			{
				long now = System.currentTimeMillis();
				wait=now+1000;
				//txtDtDOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
				txtmdate.setFocus();
				txtmdate.selectAll();
			}
			

		}
	});
	
	txtddate.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
			{
			if(!txtddate.getText().equals("") && Integer.valueOf ( txtddate.getText())<10 && txtddate.getText().length()< txtddate.getTextLimit())
			{
				txtddate.setText("0"+ txtddate.getText());
				//txtFromDtMonth.setFocus();
				txtmdate.setFocus();
				return;
				
				
				
			}
			}
			

		}
	});

	txtmdate.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtmdate.getText().length()==txtmdate.getTextLimit())
			{
				txtyrdate.setFocus();
				txtyrdate.selectAll();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtddate.setFocus();
				txtddate.selectAll();
			}
		

		}
	});

	txtmdate.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
			{
			if(!txtmdate.getText().equals("") && Integer.valueOf ( txtmdate.getText())<10 && txtmdate.getText().length()< txtmdate.getTextLimit())
			{
				txtmdate.setText("0"+ txtmdate.getText());
				//txtFromDtMonth.setFocus();
				txtyrdate.setFocus();
				return;
				
				
				
			}
			}
			

		}
	});

	
	txtyrdate.addKeyListener(new KeyAdapter() {
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		//super.keyPressed(arg0);
		if(txtyrdate.getText().length()==txtyrdate.getTextLimit())
		{
			drpdwnselectproj.setFocus();
		}
		if(arg0.keyCode==SWT.ARROW_UP)
		{
			txtmdate.setFocus();
			txtmdate.selectAll();
		}
	

	}
});
	

	
	
		txtddate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtmdate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		txtyrdate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		txtddate.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				if(!txtddate.getText().equals("") && (Integer.valueOf(txtddate.getText())> 31 || Integer.valueOf(txtddate.getText()) <= 0) )
				{
					MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
					msgbox.setMessage("you have entered an invalid date");
					msgbox.open();
					
					txtddate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
						txtddate.setFocus();
						txtddate.selectAll();
						}
					});
					return;
				}
				if(!txtddate.getText().equals("") && (Integer.valueOf(txtddate.getText())< 10 && txtddate.getText().length() < txtddate.getTextLimit()))
						{
					txtddate.setText("0"+ txtddate.getText());
					return;
						}
				if(txtddate.getText().equals(""))
				{
					txtddate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
						txtddate.setFocus();
						txtddate.selectAll();
						}
					});
					return;
				
				}
			}
		});
			
		
		txtmdate.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				if(!txtmdate.getText().equals("") && (Integer.valueOf(txtmdate.getText())> 12 || Integer.valueOf(txtmdate.getText()) <= 0) )
				{
					MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
					msgbox.setMessage("you have entered an invalid month");
					msgbox.open();
					
					txtmdate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
						txtmdate.setFocus();
						txtmdate.selectAll();
						}
					});
					return;
				}
				if(txtmdate.getText().equals(""))
				{
					txtddate.setFocus();
				}
				if(!txtmdate.getText().equals("") && (Integer.valueOf(txtmdate.getText())< 10 && txtmdate.getText().length() < txtmdate.getTextLimit()))
						{
					txtmdate.setText("0"+ txtmdate.getText());
					return;
						}
				if(txtmdate.getText().equals(""))
				{
					txtmdate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
						txtmdate.setFocus();
						txtmdate.selectAll();
						}
					});
					return;
				
				}
			}
		});
		
		txtyrdate.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				if(!txtyrdate.getText().trim().equals("") && Integer.valueOf(txtyrdate.getText()) < 1900) 
						{
					MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
					msgbox.setMessage("you have entered an invalid year");
					msgbox.open();
				
					txtyrdate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtyrdate.setFocus();
							txtyrdate.selectAll();
							
						}
					});
					
				}
				if(txtyrdate.getText().equals(""))
				{
					txtyrdate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
						txtyrdate.setFocus();
						txtyrdate.selectAll();
						}
					});
					return;
				
				}
				return;
			}
		});

		
				txtyrdate.addFocusListener(new FocusAdapter() {
		
			public void focusLost(FocusEvent args0) {
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date voucherDate = sdf.parse(txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText());
					Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
					Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
					
					if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
					{
						MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK );
						errMsg.setMessage("please enter the date within the financial year");
						errMsg.open();
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
							txtyrdate.setText("");
								txtyrdate.setFocus();
							txtyrdate.setSelection(0,4);
							}
						});
						
						return;
					}
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.getMessage();
				}
			}
		});
			
		drpdwnselectproj.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(drpdwnselectproj.getSelectionIndex()<= 0 )
				{
					btnView.setEnabled(false);
					
				}
				else
				{
					btnView.setEnabled(true);
				}
			}
		});
		drpdwnselectproj.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					if(drpdwnselectproj.getSelectionIndex()<=1)
					{
							txtyrdate.setFocus();
						
							txtyrdate.selectAll();
					}
					
				}
			
				if(drpdwnselectproj.getSelectionIndex() <= 0)
				{
					//arg0.doit= false;
					return;
				}
				if(arg0.keyCode== SWT.CR || arg0.keyCode == SWT.KEYPAD_CR )
				{
					btnView.notifyListeners(SWT.Selection, new Event() );
					
				}
			}
		});
		btnView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				//make a call to the reportController.getLedger()
				Composite grandParent = (Composite) btnView.getParent().getParent();
				//MessageBox msg = new MessageBox(new Shell(), SWT.OK);
				//msg.setMessage(grandParent.getText());
				//msg.open();
				//String fromDate = txtddate.getText() + "-" + txtFromDtMonth.getText() + "-" + txtFromDtDay.getText();
				

				
				String sp = drpdwnselectproj.getItem(drpdwnselectproj.getSelectionIndex());
				String toDate = txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText();
				
				btnView.getParent().dispose();
				gnukhata.controllers.reportController.showProjectStatement(grandParent, toDate, sp);
				dispose();
				
				
			}
			
		});
				
	}
	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
}